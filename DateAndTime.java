import java.util.Scanner;
import java.time.format.DateTimeFormatter;
import java.time.LocalDate;

public class DateAndTime {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.next();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate date = LocalDate.parse(text, formatter);
        System.out.println(date.isLeapYear() ? "Год является високосным" : "Год не является високосным");
    }
}
